const CompressionPlugin = require('compression-webpack-plugin');
const Timestamp = new Date().getTime();
module.exports = {
    devServer: {
        host: '127.0.0.1',
        port: 9999,
        proxy: {
            '/xboot': {
                target: 'http://47.112.199.23:8100/xboot',  // 请求本地 需要xboot后台项目
                ws: true
            },
            '/bi': {
                target: 'http://47.112.199.23:8100/',  // 请求本地 需要bi后台项目
                ws: true
            },
            '/omsServer': {
                target: 'http://47.112.199.23:8100/',  // 请求本地 需要bi后台项目
                ws: true
            },
        }
        // proxy: {
        //     '/xboot': {
        //         target: 'http://localhost:8888',  // 请求本地 需要xboot后台项目
        //         ws: true
        //     },
        //     '/bi': {
        //         target: 'http://47.112.199.23:8100/',  // 请求本地 需要bi后台项目
        //         ws: true
        //     },
        // }
    },
    // 打包时不生成.map文件 避免看到源码
    productionSourceMap: false,
    // 部署优化
    configureWebpack: {
        output: { // 输出重构  打包编译后的 文件名称  【模块名称.版本号.时间戳】
            filename: `js/[name].${process.env.VUE_APP_Version}.${Timestamp}.js`,
            chunkFilename: `js/[name].${process.env.VUE_APP_Version}.${Timestamp}.js`
        },
        // 使用CDN
        // externals: {
        //     vue: 'Vue',
        //     'vue-i18n': 'VueI18n',
        //     axios: 'axios',
        //     'vue-router': 'VueRouter',
        //     vuex: 'Vuex',
        //     iview: 'iview',
        //     echarts: 'echarts',
        //     apexcharts: 'ApexCharts',
        //     'vue-apexcharts': 'VueApexCharts',
        //     xlsx: 'XLSX',
        //     dplayer: 'DPlayer',
        //     gitalk: 'Gitalk',
        //     'js-cookie': 'Cookies',
        //     wangEditor: 'wangEditor'
        // },
        // GZIP压缩
        plugins: [
            new CompressionPlugin({
                test: /\.js$|\.html$|\.css/, // 匹配文件
                threshold: 10240 // 对超过10k文件压缩
            })
        ]
    }
}
