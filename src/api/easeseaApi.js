// 统一请求路径前缀在libs/axios.js中修改
import {getRequest, postRequest, postJson, putRequest, deleteRequest, getRequestWithNoToken} from '@/libs/axios';

let base = '/xboot';

export const chooseProductUploadImg = "/xboot/choose-product/file";

//获取site列表
export const backendAPI = (method, params, id) => {
    if(method){
        if(method.toUpperCase() == "GET"){
            let path = id ? `/easesea/account-backend/${id}` : '/easesea/account-backend';
            return getRequest(base, path, params)
        }else if(method.toUpperCase() == "POST"){
            return postRequest(base, '/easesea/account-backend', params)
        }else if(method.toUpperCase() == "PUT"){
            return putRequest(base, '/easesea/account-backend', params)
        }else if(method.toUpperCase() == "DELETE"){
            return deleteRequest(base, `/easesea/account-backend/${id}`, params)
        }
    }else{
        return {message:"缺少参数"}
    }
};

//获取site列表
export const backendGetAll = (params) => {
    return getRequest(base, '/easesea/account-backend/all', params)
};

//获取枚举状态
export const getBackEndStatusEnum = (params) => {
    return getRequest(base, '/easesea/account-backend/getEnum', params)
};

export const backEndUpdateStatusByIds = (params) => {
    return putRequest(base, '/easesea/account-backend/updateStatus', params)
};

export const backendImport = (params) => {
    return postJson(base, '/easesea/account-backend/import', params)
};


//获取site列表
export const domainAPI = (method, params, id) => {
    if(method){
        if(method.toUpperCase() == "GET"){
            let path = id ? `/easesea/account-domain/${id}` : '/easesea/account-domain';
            return getRequest(base, path, params)
        }else if(method.toUpperCase() == "POST"){
            return postRequest(base, '/easesea/account-domain', params)
        }else if(method.toUpperCase() == "PUT"){
            return putRequest(base, '/easesea/account-domain', params)
        }else if(method.toUpperCase() == "DELETE"){
            return deleteRequest(base, `/easesea/account-domain/${id}`, params)
        }
    }else{
        return {message:"缺少参数"}
    }
};

//获取site列表
export const domainGetAll = (params) => {
    return getRequest(base, '/easesea/account-domain/all', params)
};

//获取枚举状态
export const getDomainStatusEnum = (params) => {
    return getRequest(base, '/easesea/account-domain/getEnum', params)
};

export const domainUpdateStatusByIds = (params) => {
    return putRequest(base, '/easesea/account-domain/updateStatus', params)
};

export const domainImport = (params) => {
    return postJson(base, '/easesea/account-domain/import', params)
};
export const getUserDomain = () =>{
    return getRequest(base, '/easesea/account-domain/getUserDomain')
}


//获取site列表
export const facebookAccountAPI = (method, params, id) => {
    if(method){
        if(method.toUpperCase() == "GET"){
            let path = id ? `/easesea/account-facebook/${id}` : '/easesea/account-facebook';
            return getRequest(base, path, params)
        }else if(method.toUpperCase() == "POST"){
            return postRequest(base, '/easesea/account-facebook', params)
        }else if(method.toUpperCase() == "PUT"){
            return putRequest(base, '/easesea/account-facebook', params)
        }else if(method.toUpperCase() == "DELETE"){
            return deleteRequest(base, `/easesea/account-facebook/${id}`, params)
        }
    }else{
        return {message:"缺少参数"}
    }
};

//获取site列表
export const facebookAccountGetAll = (params) => {
    return getRequest(base, '/easesea/account-facebook/all', params)
};

//获取枚举状态
export const getFacebookAccountStatusEnum = (params) => {
    return getRequest(base, '/easesea/account-facebook/getEnum', params)
};

export const facebookAccountUpdateStatusByIds = (params) => {
    return putRequest(base, '/easesea/account-facebook/updateStatus', params)
};

export const facebookAccountImport = (params) => {
    return postJson(base, '/easesea/account-facebook/import', params)
};

export const easeseaOrderApi = {
    findOrderStatisticByTime:(params) => {return getRequest(base,'/easesea/orders/findOrderStatisticByTime', params)},

};
export const easeseAbandonedApi = {
    getCondition: (params) => {return getRequest(base, '/easesea/abandoned-orders/getByCondition', params)},
    getStatic: (params) => {return getRequest(base, '/easesea/abandoned-orders/getStatic', params)},
};

export const easeseProductsApi = {
    findSPUProductStatistic: (params)=>{ return getRequest(base, '/easesea/platform-products/findSPUProductStatistic', params)},
    findSPUProductStatisticAll: (params)=>{ return getRequest(base, '/easesea/platform-products/findSPUProductStatisticAll', params)},
    getSPUNoSaleProduct: (params)=>{ return getRequest(base, '/easesea/platform-products/getSPUNoSaleProduct', params)},
    getVariantSale: (params)=>{ return getRequest(base, '/easesea/platform-products/getVariantSale', params)},
    getProductStaticByTime: (params)=>{ return getRequest(base, '/easesea/platform-products/getProductStaticByTime', params)},
    getShoplazzaIdByUrl: (params)=>{ return getRequest(base, '/easesea/platform-products/getShoplazzaIdByUrl', params)},
    getNoSaleProduct: (params)=>{ return getRequest(base, '/easesea/platform-products/getNoSaleProduct', params)},
    get:(params)=>{return getRequest(base, '/easesea/platform-products', params)}
};

//paypalManagement APi
export const paypalApi = (method, params, id) => {
    if(method){
        if(method.toUpperCase() == "GET"){
            let path = id ? `/easesea/account-paypal/${id}` : '/easesea/account-paypal';
            return getRequest(base, path, params)
        }else if(method.toUpperCase() == "POST"){
            return postRequest(base, '/easesea/account-paypal', params)
        }else if(method.toUpperCase() == "PUT"){
            return putRequest(base, '/easesea/account-paypal', params)
        }else if(method.toUpperCase() == "DELETE"){
            return deleteRequest(base, `/easesea/account-paypal/${id}`, params)
        }
    }else{
        return {message:"缺少参数"}
    }
};

export const paypalManageApi = {
    getStatusEnum: (params)=>{ return getRequest(base, '/easesea/account-paypal/getEnum', params)},
    batchUpdateStatus: (params)=>{ return putRequest(base, '/easesea/account-paypal/updateStatus', params)},
    getAll: (params)=>{ return getRequest(base, '/easesea/account-paypal/all', params)}
};

//获取site列表
export const adAccountAPI = (method, params, id) => {
    if(method){
        if(method.toUpperCase() == "GET"){
            let path = id ? `/easesea/ad-account/${id}` : '/easesea/ad-account';
            return getRequest(base, path, params)
        }else if(method.toUpperCase() == "POST"){
            return postRequest(base, '/easesea/ad-account', params)
        }else if(method.toUpperCase() == "PUT"){
            return putRequest(base, '/easesea/ad-account', params)
        }else if(method.toUpperCase() == "DELETE"){
            return deleteRequest(base, `/easesea/ad-account/${id}`, params)
        }
    }else{
        return {message:"缺少参数"}
    }
};

export const adAccountManageAPI = {
    getStatusEnum: (params)=>{ return getRequest(base, '/easesea/ad-account/getEnum', params)},
    batchUpdateStatus: (params)=>{ return putRequest(base, '/easesea/ad-account/updateStatus', params)},
    getAll: (params)=>{ return getRequest(base, '/easesea/ad-account/all', params)}
}

export const chooseProductApi = {
    searchImages: (params)=>{ return postRequest(base, '/choose-product/searchImage', params)}
}

export const paypalRefundAPI = {
    getStatusEnum : (params) => {return getRequest(base, '/easesea/paypalApi/getRefundRequestStatus', params)},
    downLoadDetail : (params) => {return getRequest(base, '/easesea/paypalApi/downLoadDetail', params)},
    listPage : (params) => {return getRequest(base, '/easesea/paypalApi/listPage', params)},
    batchRefund : (params) => {return postJson(base, '/easesea/paypalApi/batchRefund', params)},
    cancelRequest : (params) => {return getRequest(base, '/easesea/paypalApi/cancel', params)},
}
