// 统一请求路径前缀在libs/axios.js中修改
import {getRequest, postRequest, postJson, putRequest, deleteRequest, getRequestWithNoToken} from '@/libs/axios';

let base = '/xboot';
//获取site列表
export const getAccountListData = (params) => {
    return getRequest(base, '/bu8/shopify_account/getByCondition', params)
};

export const enableAccount = (ids, params) => {
    return postRequest(base, `/bu8/shopify_account/enable/${ids}`, params)
};

export const disableAccount = (ids, params) => {
    return postRequest(base, `/bu8/shopify_account/disable/${ids}`, params)
};

export const addAccount = (params) => {
    return postRequest(base, '/bu8/shopify_account/add', params)
};

export const editAccount = (params) => {
    return postRequest(base, '/bu8/shopify_account/edit', params)
};

export const getOrderDetailListData = (params) => {
    return getRequest(base, '/bu8/ordersItems/getByCondition', params)
};
export const getOrdersStaticCount = (params) => {
    return getRequest(base, '/bu8/ordersItems/getOrdersStaticCount', params)
};
export const getOrdersStaticPrice = (params) => {
    return getRequest(base, '/bu8/ordersItems/getOrdersStaticPrice', params)
};
export const getSaleProductCount = (params) => {
    return getRequest(base, '/bu8/ordersItems/getSaleProductCount', params)
};
export const getOrderUnFulfilledRate = (params) => {
    return getRequest(base, '/bu8/orderItems/getOrderUnFulfilledRate', params)
};
export const findOrderStatisticByTime = (params) => {
    return postRequest(base, '/bu8/products/findOrderStatisticByTime', params)
};
export const findSPUProductStatisticAll = (params) => {
    return postRequest(base, '/bu8/products/findSPUProductStatisticAll', params)
};
export const getOrderInfo=(params)=>{
    return getRequest(base, '/bu8/orders/getOrderInfo', params)
}


export const getProductDetailListData = (params) => {
    return getRequest(base, '/bu8/products/getByCondition', params)
};
export const getNoSaleProduct = (params) => {
    return getRequest(base, '/bu8/products/getNoSaleProduct', params)
};
export const getSPUNoSaleProduct = (params) => {
    return getRequest(base, '/bu8/products/getSPUNoSaleProduct', params)
};
export const findSPUProductStatistic = (params) => {
    return getRequest(base, '/bu8/products/findSPUProductStatistic', params)
};
export const getVariantSale = (params) => {
    return getRequest(base, '/bu8/products/getVariantSale', params)
};
export const getProductStatistic = (params) => {
    return getRequest(base, '/bu8/products/getProductStaticByTime', params)
};
export const getShoplazzaIdByUrl = (params) =>{
    return postRequest(base, '/bu8/products/getShoplazzaIdByUrl', params)
};


export const getCheckoutDetailList = (params) => {
    return getRequest(base, '/bu8/checkouts/getByCondition', params)
};

export const getCheckoutStaticList = (params) => {
    return getRequest(base, '/bu8/checkouts/getStatic', params)
};

export const getStoreCodes = (params) => {
    return getRequest(base, '/bu8/shopify_account/getSimpleAll')
};

export const getUserStoreCodes = (params) => {
    return getRequest(base, '/bu8/shopify_account/getUserStoreCodes')
};

/**
 * 采购相关API start
 **/
export const getPurchaseList = (params) => {
    return getRequest(base, '/bu8/tongtoolPurchases/getByCondition',params)
};
export const addPurchaseOrder = (params) => {
    return postJson(base, '/bu8/tongtoolPurchases/addPurchaseOrder',params)
};
export const editPurchaseOrder = (params) => {
    return postJson(base, '/bu8/tongtoolPurchases/editPurchaseOrder',params)
};
export const editPurchase = (params) => {
    return getRequest(base, '/bu8/tongtoolPurchases/editPurchase',params)
};
export const getPurchaseDetailPage = (params) => {
    return getRequest(base, '/bu8/tongtoolPurchases/getPurchaseDetailPage',params)
};
export const updateErrorFinish = (params) => {
    return postRequest(base, `/bu8/tongtoolPurchases/updateErrorFinish`,params)
};
export const deletePurchaseByOrderNumber = (params) => {
    return postRequest(base, '/bu8/tongtoolPurchases/deleteOrderNumber',params)
};
export const getHistoryOrders = (params) => {
    return getRequest(base, `/bu8/tongtoolPurchases/getHistoryOrders`,params)
};
export const savePurchaseException = (params) => {
    return postJson(base, '/bu8/purchaseException/saveException',params)
};
export const nextException = (params) => {
    return postJson(base, '/bu8/purchaseException/nextException',params)
};
export const closeException = (params) => {
    return postJson(base, '/bu8/purchaseException/closeException',params)
};
export const getExceptionType = (params) => {
    return getRequest(base, '/bu8/purchaseException/getExceptionType',params)
};
export const getExceptionList = (params) => {
    return getRequest(base, '/bu8/purchaseException/getExceptionList',params)
};
export const getExceptionDetail = (id, params) => {
    return getRequest(base, `/bu8/purchaseException/getExceptionDetail/${id}`,params)
};
export const getSelfExceptionList = (params) =>{
    return getRequest(base, '/bu8/purchaseException/getSelfExceptionList',params)
};
export const getPurchaseDetailPageList = (params) =>{
    return getRequest(base, '/bu8/purchase/getByCondition',params)
};
export const getPurchaseDetailAll = (params) =>{
    return getRequest(base, '/bu8/purchase/getAll',params)
};
/**
 * end
 **/

/**
 * 供应商相关start
 */
export const getSupplierList = (params) => {
    return getRequest(base, `/bu8/supplier/getByCondition`,params)
};
export const getAllSupplier = (params) => {
    return getRequest(base, `/bu8/supplier/getAll`,params)
};
export const updatePurchaser = (params) => {
    return postRequest(base, `/bu8/supplier/updatePurchaser`,params)
};
export const getSupplier = (params) => {
    return getRequest(base, `/bu8/supplier/getSupplier`,params)
};
export const addSupplier = (params) => {
    return postRequest(base, `/bu8/supplier/addSupplier`,params)
};
export const deleteSupplier = (params) => {
    return postRequest(base, `/bu8/supplier/deleteSupplier`,params)
};
export const editSupplier = (params) => {
    return postRequest(base, `/bu8/supplier/editSupplier`,params)
};
export const getUserByPurchase = (params) => {
    return getRequest(base, `/bu8/supplier/getUserByPurchase`,params)
};
export const getStaticByPurchaser =(params) => {
    return getRequest(base, `/bu8/supplier/getStaticByPurchaser`, params)
}
export const importSupplierData = (params) =>{
    return postJson(base, `/bu8/supplier/importSupplier`, params)
}
/**
 * 供应商相关end
 */
