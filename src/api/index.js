// 统一请求路径前缀在libs/axios.js中修改
import { getRequest, postRequest,postJson, putRequest, deleteRequest, getRequestWithNoToken,getJsonNoBaseNoToken } from '@/libs/axios';


let base = '/xboot';
// 文件上传接口
export const uploadFile = "/xboot/upload/file"
// 验证码渲染图片接口
export const drawCodeImage = "/xboot/common/captcha/draw/"
// 获取菜单
export const getMenuList = "/xboot/permission/getMenuList"
// 获取数据字典
export const getDictData = "/xboot/dictData/getByType/"
// 获取数据字典
export const getDictDataList = (params) => {
    return getRequest(base, '/dictData/getByTypeList', params)
}
// 登陆
export const login = (params) => {
    return postRequest(base, '/login', params)
}
// 获取用户登录信息
export const userInfo = (params) => {
    return getRequest(base, '/user/info', params)
}
// 注册
export const regist = (params) => {
    return postRequest(base, '/user/regist', params)
}
// 初始化验证码
export const initCaptcha = (params) => {
    return getRequestWithNoToken(base, '/common/captcha/init', params)
}
// 发送短信验证码
export const sendSms = (mobile, params) => {
    return getRequest(base, `/common/captcha/sendSms/${mobile}`, params)
}
// 短信验证码登录
export const smsLogin = (params) => {
    return postRequest(base, '/user/smsLogin', params)
}
// IP天气信息
export const ipInfo = (params) => {
    return getRequest(base, '/common/ip/info', params)
}
// 个人中心编辑
export const userInfoEdit = (params) => {
    return postJson('/user/edit', params)
}
// 个人中心修改密码
export const changePass = (params) => {
    return postRequest(base, '/user/modifyPass', params)
}
// 解锁
export const unlock = (params) => {
    return postRequest(base, '/user/unlock', params)
}



// github登录
export const githubLogin = (params) => {
    return getRequest(base, '/social/github/login', params)
}
// qq登录
export const qqLogin = (params) => {
    return getRequest(base, '/social/qq/login', params)
}
// 微博登录
export const weiboLogin = (params) => {
    return getRequest(base, '/social/weibo/login', params)
}
// 绑定账号
export const relate = (params) => {
    return postRequest(base, '/social/relate', params)
}
// 获取JWT
export const getJWT = (params) => {
    return getRequest(base, '/social/getJWT', params)
}



// 获取绑定账号信息
export const relatedInfo = (username, params) => {
    return getRequest(base, `/relate/getRelatedInfo/${username}`, params)
}
// 解绑账号
export const unRelate = (params) => {
    return postRequest(base, '/relate/delByIds', params)
}
// 分页获取绑定账号信息
export const getRelatedListData = (params) => {
    return getRequest(base, '/relate/findByCondition', params)
}



// 获取用户数据 多条件
export const getUserListData = (params) => {
    return getRequest(base, '/user/getByCondition', params)
}
// 获取全部用户数据
export const getAllUserData = (params) => {
    return getRequest(base, '/user/getAll', params)
}
// 添加用户
export const addUser = (params) => {
    return postRequest(base, '/user/admin/add', params)
}
// 编辑用户
export const editUser = (params) => {
    return postRequest(base, '/user/admin/edit', params)
}
// 启用用户
export const enableUser = (id, params) => {
    return postRequest(base, `/user/admin/enable/${id}`, params)
}
// 禁用用户
export const disableUser = (id, params) => {
    return postRequest(base, `/user/admin/disable/${id}`, params)
}
// 删除用户
export const deleteUser = (ids, params) => {
    return deleteRequest(base, `/user/delByIds/${ids}`, params)
}
// 重置用户密码
export const resetUserPass = (params) => {
    return postRequest(base, '/user/resetPass', params)
}



// 获取一级部门
export const initDepartment = (params) => {
    return getRequest(base, '/department/getByParentId/0', params)
}
// 加载部门子级数据
export const loadDepartment = (id, params) => {
    return getRequest(base, `/department/getByParentId/${id}`, params)
}
// 通过部门获取全部用户数据
export const getUserByDepartmentId = (id, params) => {
    return getRequest(base, `/user/getByDepartmentId/${id}`, params)
}
// 添加部门
export const addDepartment = (params) => {
    return postRequest(base, '/department/add', params)
}
// 编辑部门
export const editDepartment = (params) => {
    return postRequest(base, '/department/edit', params)
}
// 删除部门
export const deleteDepartment = (ids, params) => {
    return deleteRequest(base, `/department/delByIds/${ids}`, params)
}
// 搜索部门
export const searchDepartment = (params) => {
    return getRequest(base, '/department/search', params)
}

export const getDomainIdByDepId = (id, params) => {
    return getRequest(base, `/department/getDomainIdByDepId/${id}`, params)
}


// 获取全部角色数据
export const getAllRoleList = (params) => {
    return getRequest(base, '/role/getAllList', params)
}
// 分页获取角色数据
export const getRoleList = (params) => {
    return getRequest(base, '/role/getAllByPage', params)
}
// 添加角色
export const addRole = (params) => {
    return postRequest(base, '/role/save', params)
}
// 编辑角色
export const editRole = (params) => {
    return postJson('/role/edit', params)
}
// 设为或取消注册角色
export const setDefaultRole = (params) => {
    return postRequest(base, '/role/setDefault', params)
}
// 分配角色权限
export const editRolePerm = (params) => {
    return postRequest(base, '/role/editRolePerm', params)
}
// 分配角色数据权限
export const editRoleDep = (params) => {
    return postRequest(base, '/role/editRoleDep', params)
}
// 删除角色
export const deleteRole = (ids, params) => {
    return deleteRequest(base, `/role/delAllByIds/${ids}`, params)
}



// 获取全部字典
export const getAllDictList = (params) => {
    return getRequest(base, '/dict/getAll', params)
}
// 添加字典
export const addDict = (params) => {
    return postRequest(base, '/dict/add', params)
}
// 编辑字典
export const editDict = (params) => {
    return postRequest(base, '/dict/edit', params)
}
// 删除字典
export const deleteDict = (ids, params) => {
    return deleteRequest(base, `/dict/delByIds/${ids}`, params)
}
// 搜索字典
export const searchDict = (params) => {
    return getRequest(base, '/dict/search', params)
}
// 获取全部字典数据
export const getAllDictDataList = (params) => {
    return getRequest(base, '/dictData/getByCondition', params)
}
// 添加字典数据
export const addDictData = (params) => {
    return postRequest(base, '/dictData/add', params)
}
// 编辑字典数据
export const editDictData = (params) => {
    return postRequest(base, '/dictData/edit', params)
}
// 删除字典数据
export const deleteData = (ids, params) => {
    return deleteRequest(base, `/dictData/delByIds/${ids}`, params)
}
// 通过类型获取字典数据
export const getDictDataByType = (type, params) => {
    return getRequest(base, `/dictData/getByType/${type}`, params)
}



// 获取全部权限数据
export const getAllPermissionList = (params) => {
    return getRequest(base, '/permission/getAllList', params)
}
// 添加权限
export const addPermission = (params) => {
    return postRequest(base, '/permission/add', params)
}
// 编辑权限
export const editPermission = (params) => {
    return postJson(base, '/permission/edit', params)
}
// 删除权限
export const deletePermission = (ids, params) => {
    return deleteRequest(base, `/permission/delByIds/${ids}`, params)
}
// 搜索权限
export const searchPermission = (params) => {
    return getRequest(base, '/permission/search', params)
}


// 分页获取日志数据
export const getLogListData = (params) => {
    return getRequest(base, '/log/getAllByPage', params)
}
// 删除日志
export const deleteLog = (ids, params) => {
    return deleteRequest(base, `/log/delByIds/${ids}`, params)
}
// 清空日志
export const deleteAllLog = (params) => {
    return deleteRequest(base, '/log/delAll', params)
}



// 分页获取Redis数据
export const getRedisData = (params) => {
    return getRequest(base, '/redis/getAllByPage', params)
}
// 通过key获取Redis信息
export const getRedisByKey = (key, params) => {
    return getRequest(base, `/redis/getByKey/${key}`, params)
}
// 获取Redis键值数量
export const getRedisKeySize = (params) => {
    return getRequest(base, '/redis/getKeySize', params)
}
// 获取Redis内存
export const getRedisMemory = (params) => {
    return getRequest(base, '/redis/getMemory', params)
}
// 获取Redis信息
export const getRedisInfo = (params) => {
    return getRequest(base, '/redis/info', params)
}
// 添加编辑Redis
export const saveRedis = (params) => {
    return postRequest(base, '/redis/save', params)
}
// 删除Redis
export const deleteRedis = (params) => {
    return deleteRequest(base, '/redis/delByKeys', params)
}
// 清空Redis
export const deleteAllRedis = (params) => {
    return deleteRequest(base, '/redis/delAll', params)
}



// 分页获取定时任务数据
export const getQuartzListData = (params) => {
    return getRequest(base, '/quartzJob/getAllByPage', params)
}
// 添加定时任务
export const addQuartz = (params) => {
    return postRequest(base, '/quartzJob/add', params)
}
// 编辑定时任务
export const editQuartz = (params) => {
    return postRequest(base, '/quartzJob/edit', params)
}
// 暂停定时任务
export const pauseQuartz = (params) => {
    return postRequest(base, '/quartzJob/pause', params)
}
// 恢复定时任务
export const resumeQuartz = (params) => {
    return postRequest(base, '/quartzJob/resume', params)
}
// 删除定时任务
export const deleteQuartz = (ids, params) => {
    return deleteRequest(base, `/quartzJob/delByIds/${ids}`, params)
}



// 分页获取消息数据
export const getMessageData = (params) => {
    return getRequest(base, '/message/getByCondition', params)
}
// 添加消息
export const addMessage = (params) => {
    return postRequest(base, '/message/add', params)
}
// 编辑消息
export const editMessage = (params) => {
    return postRequest(base, '/message/edit', params)
}
// 删除消息
export const deleteMessage = (ids, params) => {
    return deleteRequest(base, `/message/delByIds/${ids}`, params)
}
// 分页获取消息推送数据
export const getMessageSendData = (params) => {
    return getRequest(base, '/messageSend/getByCondition', params)
}
// 编辑发送消息
export const editMessageSend = (params) => {
    return putRequest(base, '/messageSend/update', params)
}
// 删除发送消息
export const deleteMessageSend = (ids, params) => {
    return deleteRequest(base, `/messageSend/delByIds/${ids}`, params)
}



// base64上传
export const base64Upload = (params) => {
    return postRequest(base, '/upload/file', params)
}
//bing背景
export const getBingImg = () =>{
    return getRequestWithNoToken(base, '/common/login/backimage')
}
