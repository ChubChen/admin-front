import { getRequest } from '@/libs/axios';

let base = "/omsServer";
const omsAPI = {
    //查询物流信息
    getOrderInfo:(params) => getRequest(base, '/web/orders/getOrderByExternalOrderId', params),

};
export default omsAPI;
