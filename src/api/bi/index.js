import { getRequest } from '@/libs/axios';

let base = "/bi";
const biServerAPI = {
    //查询销售额
    getSaleStaticsByHour:(params) => getRequest(base, '/order/saleStatisticsByHour', params),
    getSaleAmountTotal:(params) => getRequest(base, '/order/getSaleAmountTotal', params),
    getOrderByHour:(params) => getRequest(base, '/order/getOrderByHour', params),
    getSumOrderAndPrice:(params) => getRequest(base, '/order/getSumOrderAndPrice', params),
    getProductByHour:(params) => getRequest(base, '/order/getProductByHour', params),
    countProductAndAvgPrice:(params) => getRequest(base, '/order/countProductAndAvgPrice', params),
    getStoreByHour:(params) => getRequest(base, '/order/getStoreByHour', params),
    countStoreAndAvgPrice:(params) => getRequest(base, '/order/countStoreAndAvgPrice', params),
    getDayToDayValue:(params) => getRequest(base, '/order/getDayToDayValue', params),
    getGroupRate:(params) => getRequest(base, '/order/getGroupRate', params),
    getTopSaleSubGroup:(params) => getRequest(base, '/order/getTopSaleSubGroup', params),
    getTopVendor:(params) => getRequest(base, '/order/getTopVendor', params),
    getTopSpuPrice:(params) => getRequest(base, '/order/getTopSpuPrice', params),
    getTopSpuCount:(params) => getRequest(base, '/order/getTopSpuCount', params),
    getTopCategory:(params) => getRequest(base, '/order/getTopCategory', params),
    getTop90Sales :(params) => getRequest(base, '/topSpu/getTop90Sales', params),
    getNewTop30Sales :(params) => getRequest(base, '/topSpu/getNewTop30Sales', params),
    getDistinctGroupName :(params) => getRequest(base, '/order/getDistinctGroupName', params),
};
export default biServerAPI;
