// 统一请求路径前缀在libs/axios.js中修改
import {getJson, getRequest, postRequest, postJson, putRequest, deleteRequest, getRequestWithNoToken} from '@/libs/axios';

let base = '/xboot/starlink';

export const domainManageAPI = {
    get: (params, id) => { let path = id ? `/account-domain/${id}` : '/account-domain';
                            return getRequest(base, path , params)},
    getAll: (params) => {return getRequest(base, '/account-domain/all', params)},
    post: (params) => {return postRequest(base, '/account-domain', params)},
    put: (params) => {return putRequest(base, '/account-domain', params)},
    deleteR: (params) => {return deleteRequest(base, '/account-domain', params)},
    getStatusEnum: (params)=>{ return getRequest(base, '/account-domain/getEnum', params)},
    batchUpdateStatus: (params)=>{ return putRequest(base, '/account-domain/updateStatus', params)},
};

export const adsBmManageAPI = {
    get: (params, id) => { let path = id ? `/account-ad-manager/${id}` : '/account-ad-manager';
        return getRequest(base, path , params)},
    getAll: (params) => {return getRequest(base, '/account-ad-manager/all', params)},
    post: (params) => {return postRequest(base, '/account-ad-manager', params)},
    put: (params) => {return putRequest(base, '/account-ad-manager', params)},
    deleteR: (params) => {return deleteRequest(base, '/account-ad-manager', params)},
    getStatusEnum: (params)=>{ return getRequest(base, '/account-ad-manager/getEnum', params)},
    batchUpdateStatus: (params)=>{ return putRequest(base, '/account-ad-manager/updateStatus', params)},
};

export const accountCompanyAPI = {
    get: (params, id) => { let path = id ? `/account-company/${id}` : '/account-company';
        return getRequest(base, path , params)},
    getAll: (params) => {return getRequest(base, '/account-company/all', params)},
    post: (params) => {return postRequest(base, '/account-company', params)},
    put: (params) => {return putRequest(base, '/account-company', params)},
    deleteR: (params) => {return deleteRequest(base, '/account-company', params)},
    getStatusEnum: (params)=>{ return getRequest(base, '/account-company/getEnum', params)},
    batchUpdateStatus: (params)=>{ return putRequest(base, '/account-company/updateStatus', params)},
};

export const adAccountAPI = {
    get: (params, id) => { let path = id ? `/account-ad/${id}` : '/account-ad';
        return getRequest(base, path , params)},
    getAll: (params) => {return getRequest(base, '/account-ad/all', params)},
    post: (params) => {return postRequest(base, '/account-ad', params)},
    put: (params) => {return putRequest(base, '/account-ad', params)},
    deleteR: (params) => {return deleteRequest(base, '/account-ad', params)},
    getStatusEnum: (params)=>{ return getRequest(base, '/account-ad/getEnum', params)},
    batchUpdateStatus: (params)=>{ return putRequest(base, '/account-ad/updateStatus', params)},
    add: (params)=>{return postJson(base, '/account-ad/add', params)}
};

export const adAppliedRecordAPI = {
    get: (params, id) => { let path = id ? `/ad-apply-record/${id}` : '/ad-apply-record';
        return getRequest(base, path , params)},
    getAll: (params) => {return getRequest(base, '/ad-apply-record/all', params)},
    post: (params) => {return postRequest(base, '/ad-apply-record', params)},
    put: (params) => {return putRequest(base, '/ad-apply-record', params)},
    deleteR: (params) => {return deleteRequest(base, '/ad-apply-record', params)},
    getStatusEnum: (params)=>{ return getRequest(base, '/ad-apply-record/getEnum', params)},
    batchUpdateStatus: (params)=>{ return putRequest(base, '/ad-apply-record/updateStatus', params)},
};
