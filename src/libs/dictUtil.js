import { getDictDataList } from '@/api/index';

let dictUtil = {
    typeList:["sex","platform","storePlan","trueOrFalse"],
};

// 获取常用的数据字典保存至vuex
dictUtil.initDictData = function (vm) {
    let param = {type:dictUtil.typeList};
    getDictDataList(param).then(res => {
        if(res.success && res.result){
            let dict = {};
            res.result.forEach((item)=>{
                if(dict[item.dictType]){
                    dict[item.dictType].push(item);
                }else{
                    dict[item.dictType] = [item];
                }
            })
            vm.$store.commit("setDict", dict);
        }
    });
};

export default dictUtil;
