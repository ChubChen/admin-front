import { getStore } from './storage';

const hasRole = {
    install (Vue, options) {
        Vue.directive('hasRole', {
            inserted (el, binding) {
                let roles = getStore("roles");
                if(typeof binding.value === "string"){
                    if (roles&&!roles.includes(binding.value)) {
                        el.parentNode.removeChild(el);
                    }
                }else if(binding.value instanceof Array ){
                    let flag = false;
                    binding.value.forEach((item)=>{
                        if (roles&&roles.includes(item)) {
                            flag = true;
                        }
                    });
                    if(!flag){
                        el.parentNode.removeChild(el);
                    }
                }
            }
        });
    }
};

export default hasRole;
