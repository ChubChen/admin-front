const dict = {
    state: {
        type:{},
    },
    mutations: {
        // 设置值的改变方法
        setDict(state, dict) {
            state.type = dict;
        },
    }
};

export default dict;
