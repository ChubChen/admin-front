/* eslint-disable */
import { saveAs } from 'file-saver'
import json2csv from "json2csv";

export const export_json_to_csv = (jsonArray, filename)=>{
    const result = json2csv.parse(jsonArray);
    saveAs(new Blob([result], {
        type: "text/csv"
    }), `${filename}.csv`);
}
